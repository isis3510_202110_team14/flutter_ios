class CustomLocation {
  double latitude, longitude;
  CustomLocation({this.latitude, this.longitude});
}

class Service {
  DateTime acceptingDate, endingDate, requestingDate, startingDate;
  String dog_walker, status, comment, user;
  double duration, rating, cost;
  CustomLocation location, walker_location;
  List dogs;

  Service({
    this.acceptingDate,
    this.comment,
    this.cost,
    this.dog_walker,
    this.dogs,
    this.duration,
    this.endingDate,
    this.location,
    this.walker_location,
    this.rating,
    this.requestingDate,
    this.startingDate,
    this.status,
    this.user,
  });

  factory Service.fromJSON(Map<dynamic, dynamic> parsedJSON, String elementId) {
    return Service(
      acceptingDate: parsedJSON["acceptingDate"] == ""
          ? null
          : DateTime.parse(parsedJSON["acceptingDate"]),
      comment: parsedJSON["comment"],
      cost: parsedJSON["cost"].toDouble(),
      dog_walker: parsedJSON["dogWalker"],
      dogs: parsedJSON["dogs"],
      duration: parsedJSON["duration"] == ""
          ? null
          : parsedJSON["duration"].toDouble(),
      endingDate: parsedJSON["endingDate"] == ""
          ? null
          : DateTime.parse(parsedJSON["endingDate"]),
      location: new CustomLocation(
          longitude: parsedJSON['location']['latitude'] == ""
              ? null
              : parsedJSON['location']['latitude'],
          latitude: parsedJSON["location"]["longitude"] == ""
              ? null
              : parsedJSON["location"]["longitude"]),
      walker_location: new CustomLocation(
          longitude: parsedJSON['petWalkerLocation']['latitude'] == ""
              ? null
              : parsedJSON['petWalkerLocation']['latitude'].toDouble(),
          latitude: parsedJSON["petWalkerLocation"]["longitude"] == ""
              ? null
              : parsedJSON["petWalkerLocation"]["longitude"].toDouble()),
      rating:
          parsedJSON["rating"] == "" ? null : parsedJSON["rating"].toDouble(),
      requestingDate: parsedJSON["requestingDate"] == ""
          ? null
          : DateTime.parse(parsedJSON["requestingDate"]),
      startingDate: parsedJSON["startingDate"] == ""
          ? null
          : DateTime.parse(parsedJSON["startingDate"]),
      status: parsedJSON['status'],
      user: parsedJSON["user"],
    );
  }
}
