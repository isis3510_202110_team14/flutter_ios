class PupheUser {
  String name, email, birthday, image;
  PupheUser({
    this.birthday,
    this.email,
    this.image,
    this.name,
  });

  factory PupheUser.fromJSON(Map<dynamic, dynamic> parsedJSON) {
    return PupheUser(
      birthday: parsedJSON['birthday'],
      email: parsedJSON['email'],
      image: parsedJSON['image'],
      name: parsedJSON['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "birthday": birthday,
      "email": email,
      "image": image,
      "name": name,
    };
  }
}
