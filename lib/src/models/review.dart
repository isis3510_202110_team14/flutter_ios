import 'package:puphe_ios/src/models/puphe_user.dart';

class Review {
  String body;
  double rating;
  PupheUser user;

  Review({this.body, this.rating, this.user});

  factory Review.fromJSON(Map<dynamic, dynamic> parsedJSON) {
    return Review(
      body: parsedJSON['body'],
      rating: parsedJSON['rating'].toDouble(),
      user: new PupheUser.fromJSON(parsedJSON['user']),
    );
  }
}
