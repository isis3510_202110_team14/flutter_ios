class Dog {
  String name, breed, bio, image, id;
  Dog({
    this.id,
    this.bio,
    this.breed,
    this.image,
    this.name,
  });

  factory Dog.fromJSON(Map<dynamic, dynamic> parsedJSON, String elementId) {
    return Dog(
      id: elementId,
      bio: parsedJSON['bio'],
      breed: parsedJSON['breed'],
      image: parsedJSON['image'],
      name: parsedJSON['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "bio": bio,
      "breed": breed,
      "image": image,
      "name": name,
    };
  }
}
