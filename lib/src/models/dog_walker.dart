class DogWalker {
  String name, email, bio, image, phone_number, birthday;
  double rating;
  int number_walks;

  DogWalker({
    this.bio,
    this.birthday,
    this.email,
    this.image,
    this.name,
    this.number_walks,
    this.phone_number,
    this.rating,
  });

  factory DogWalker.fromJSON(Map<dynamic, dynamic> parsedJSON) {
    return DogWalker(
      bio: parsedJSON['bio'],
      birthday: parsedJSON['birthday'],
      email: parsedJSON['email'],
      image: parsedJSON['image'],
      name: parsedJSON['name'],
      number_walks: parsedJSON['number_walks'],
      phone_number: parsedJSON['phone_number'],
      rating: parsedJSON['rating'].toDouble(),
    );
  }
}
