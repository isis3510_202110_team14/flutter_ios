import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:puphe_ios/src/functionalities/daily_reminder.dart';
import 'package:puphe_ios/src/models/service.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:rating_dialog/rating_dialog.dart';

class MyRatingDialog extends StatelessWidget {
  final String user;
  final Service service;
  final DailyReminder reminderer = new DailyReminder();
  MyRatingDialog({Key key, String user, Service service})
      : this.user = user,
        this.service = service,
        super(key: key);

  ServiceFacade serviceFacade = new ServiceFacade();
  finishService(Service newService) async {
    await serviceFacade.finishService(user, newService);
  }

  @override
  Widget build(BuildContext context) {
    Future<Uint8List> getMarket() async {
      ByteData byteData =
          await DefaultAssetBundle.of(context).load("assets/dog_walker.png");
      Uint8List imageData2 = await byteData.buffer.asUint8List();
      return imageData2;
    }

    return RatingDialog(
      // your app's name?
      title: 'Rating your experience',
      // encourage your user to leave a high rating?
      message:
          'Tap a star to set your rating. Add more description here if you want.',
      // your app's logo?
      image: Image.asset('assets/dog_walker.png', height: 100),
      submitButton: 'Rate',
      onCancelled: () {
        try {
          reminderer.schedule(service.user, service.dogs[0]);
        } catch (e) {
          print(e);
        }
        service.comment = "No comment";
        finishService(service);
      },
      onSubmitted: (response) {
        try {
          reminderer.schedule(service.user, service.dogs[0]);
        } catch (e) {
          print(e);
        }
        service.comment = response.comment;
        service.rating = response.rating.toDouble();
        finishService(service);
      },
    );
  }
}
