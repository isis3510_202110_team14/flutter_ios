import 'dart:typed_data';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:puphe_ios/src/models/service.dart';
import 'package:puphe_ios/src/screens/map_widgets/bottom_card.dart';
import 'package:flutter/services.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:connectivity/connectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Map extends StatefulWidget {
  @override
  State<Map> createState() => _MapState();
}

class _MapState extends State<Map> {
  String status = "";
  StreamSubscription _locationSubscription;
  Location _tracker = Location();
  Marker marker;
  Circle circle;
  GoogleMapController _googleMapController;
  ServiceFacade _serviceFacade = ServiceFacade();
  String usuario;
  Marker service;
  Marker petWalker;
  bool allowedTo = true;
  Uint8List imageData;
  Service walkerService;
  bool isConnected = false;
  CameraPosition initialLocation;

  @override
  void initState(){
  super.initState();
   _serviceFacade.setUserSession().then((value) {
    setState(() {
     usuario = value;
    });
  });
  Uint8List img;
  getMarket("assets/dog.png").then((value) => setState(()=>img=value));
  _tracker.getLocation().then((value){
    updateMarker(value, img);
    setState(() {
      initialLocation = new CameraPosition(
          bearing: 192.8,
          target: LatLng(value.latitude, value.longitude),
          tilt: 0,
          zoom: 18);

    });
  });
  }

  Future<Uint8List> getMarket(path) async {
    ByteData byteData = await DefaultAssetBundle.of(context).load(path);
    return byteData.buffer.asUint8List();
  }

  void updateMarker(LocationData newLocalData, Uint8List imageData) {
    LatLng latLng = LatLng(newLocalData.latitude, newLocalData.longitude);
    this.setState(() {
      marker = Marker(
        markerId: MarkerId("Home"),
        position: latLng,
        rotation: newLocalData.heading,
        draggable: false,
        zIndex: 2,
        flat: true,
        anchor: Offset(0.5, 0.5),
        icon: BitmapDescriptor.fromBytes(imageData),
      );
      circle = Circle(
          circleId: CircleId("PetCircle"),
          radius: newLocalData.accuracy * 3,
          zIndex: 1,
          strokeColor: Color(0x90B8F9),
          center: latLng,
          fillColor: Color(0x448AFF).withAlpha(70));
    });
  }

  void _getLocation(bool input) async {
    Marker x;
    bool click = false;
    click = input;
    try {
      Uint8List imageData2 = await getMarket("assets/dog.png");

      var location = await _tracker.getLocation();
      updateMarker(location, imageData);
      if (_locationSubscription != null) {
        _locationSubscription.cancel();
      }
      _locationSubscription = _tracker.onLocationChanged.listen((newData) {
        if (_googleMapController != null) {
          if (click == true) {
            _googleMapController.animateCamera(CameraUpdate.newCameraPosition(
                new CameraPosition(
                    bearing: 192.8,
                    target: LatLng(newData.latitude, newData.longitude),
                    tilt: 0,
                    zoom: 18)));
            click = false;
          }
          updateMarker(newData, imageData2);
        }
      });
    } on PlatformException catch (e) {
      if (e.code == "PERMISSION_DENIED") {
        debugPrint("Permiso Denegado");
      }
    }
    await getServiceStatus();
  }

  void _addMarkerOnTap(LatLng latlang) {
    this.setState(() {
      service = (Marker(
          markerId: MarkerId("Position4"),
          draggable: false,
          zIndex: 2,
          flat: true,
          position: latlang,
          icon: BitmapDescriptor.defaultMarker));
    });
  }

  @override
  void dispose() {
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    super.dispose();
  }

  Widget diplayBottomCard() {
    if (service == null) {
      return BottomCard(lat: null, long: null, callBack: (String status) {  },);
    } else {
      return BottomCard(
          lat: service.position.latitude,
          long: service.position.longitude,
          callBack: (String callBack) {
            setState(() {
              this.status = callBack;
            });
          });
    }
  }

  // ignore: non_constant_identifier_names
  Widget WidgetServiceState() {
    if (status == "No service") {
      setState(() {
        allowedTo = true;
      });
      return diplayBottomCard();
    } else if (status == "Waiting") {
      setState(() {
        allowedTo = false;
      });

      return WaitingBottomCard(
        status: status,
        user: usuario,
        callback: (String statuses) {
          setState(() {
            status = statuses;
          });
        },
      );
    } else if (status == "Ongoing" || status == "Accepted" || status=="Returning") {
      return ServiceBottomCard(status: status, user: usuario, callback: (Service service) {  },);
    } else if (status == "Finished") {
      return FinishedBottomCard(user: usuario, service: walkerService);
    }
    return diplayBottomCard();
  }


  Future<bool> checkConnectivity() async {
    bool connected;
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      connected = true;
    } else {
      connected = false;
    }
    setState(() {
      isConnected = connected;
    });
     getServiceStatus();

    return connected;
  }

  getServiceStatus() async {
    String uservariable;
    Marker x=null;
    Marker y=null ;
    Service serviceStatus;

    await _serviceFacade.setUserSession().then((value) {
      uservariable=value;
    });
    Uint8List imageData2 = await getMarket("assets/dog_walker.png");
    if(this.mounted){
    setState(() {
       imageData = imageData2;
     });}
    await _serviceFacade.getUserService(usuario).then((Service value) {
      if (value != null) {
        LatLng latlng =
        LatLng(value.location.longitude, value.location.latitude);
         y = Marker(
            markerId: MarkerId("Position5"),
            draggable: false,
            zIndex: 2,
            flat: true,
            position: latlng,
            icon: BitmapDescriptor.defaultMarker);
        if (value.status != "Waiting" && value.status != "Finished") {
          LatLng latLng2 = LatLng(
              value.walker_location.longitude, value.walker_location.latitude);
           x = Marker(
            markerId: MarkerId("PetWalker"),
            position: latLng2,
            draggable: false,
            zIndex: 3,
            flat: true,
            anchor: Offset(0.5, 0.5),
            icon: BitmapDescriptor.fromBytes(imageData),
          );
        }
        serviceStatus=value;
      }
    });
    if(this.mounted){
      setState((){
        usuario=uservariable;
        walkerService=serviceStatus;
        if(walkerService!=null){
        status=walkerService.status;
        if (serviceStatus.status == "Waiting" || serviceStatus.status == "Finished") {
          petWalker = null;
          allowedTo = false;
        }
        if (serviceStatus.status == "Ongoing" || serviceStatus.status == "Accepted") {
          allowedTo = false;
          petWalker=x;
          service=y;
        }
        }
      });
    }
  }

  Set<Marker> getMarkers(){
    Set<Marker> set ={};
      if (marker != null && service != null && petWalker != null) {
        set = {marker,service,petWalker};
      } else if (marker != null && service == null && petWalker != null) {
        set= {marker,petWalker};
      } else if (marker == null && service != null && petWalker != null) {
        set = {service,petWalker};
      } else if (marker == null && service != null && petWalker == null) {
        set = {service};
      } else if (marker != null && service == null && petWalker == null) {
        set = {marker};
      } else if (marker != null && service != null && petWalker == null) {
        set = {marker,service};
      } else if (marker == null && service != null && petWalker == null) {
        set = {service};
      } else if (marker == null && service == null && petWalker != null) {
        set = {petWalker};
      }

    return set;
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder2 = new FutureBuilder(
        future: getServiceStatus(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return new Container();
            case ConnectionState.waiting:
              return new Container();
            default:
              if (snapshot.hasError) {
                return new Container();
              } else {
                return new Container();
              }
          }
        });
    var futureBuilder = new FutureBuilder(
      future: checkConnectivity(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return new Container();
          case ConnectionState.waiting:
            return new Container();
          default:
            if (snapshot.hasError) {
              return new Container();
            } else {
              return new Container();
            }
        }
      },
    );

    return isConnected
        ? CupertinoPageScaffold(
            navigationBar: CupertinoNavigationBar(
              backgroundColor: Colors.transparent,
              middle: const Text(
                  'Tap the location where you want to take the service!',
                  style: TextStyle(color: Colors.grey)),
              leading: CupertinoNavigationBarBackButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                color: Colors.white,
              ),
            ),
            child: Material(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  initialLocation!=null?GoogleMap(
                    compassEnabled: false,
                    initialCameraPosition: initialLocation,
                    markers: getMarkers(),
                    circles: Set.of((circle != null) ? [circle] : []),
                    onMapCreated: (GoogleMapController controller) {
                      _googleMapController = controller;
                    },
                    onTap: (latlang) {
                      if (allowedTo) {
                        _addMarkerOnTap(latlang);
                      }
                    },
                  ):Container(
                      child: Center(child: CircularProgressIndicator())),
                  Positioned(
                    left: MediaQuery.of(context).size.width / 1.8 + 100,
                    top: MediaQuery.of(context).size.height / 8,
                    child: Container(
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                Color(0xffa6f2db),
                              ),
                              shape: MaterialStateProperty.all<CircleBorder>(
                                  CircleBorder())),
                          child: Icon(Icons.location_searching),
                          onPressed: () {
                            _getLocation(true);
                          }),
                    ),
                  ),
                  WidgetServiceState(),
                  futureBuilder2 == null ? Container() : futureBuilder2,
                  futureBuilder == null ? Container() : futureBuilder,
                ].toList(),
              ),
            ),
          )
        : Container(
            child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.wifi_off),
                Image.asset("assets/dog_walker.png"),
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Text(
                      'Please connect to internet!!!',
                      style: CupertinoTheme.of(context)
                          .textTheme
                          .navLargeTitleTextStyle,
                    )),
                SizedBox(height: 32),
              ].toList(),
            ),
          ));
  }
}
