import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puphe_ios/src/models/dog_walker.dart';
import 'package:puphe_ios/src/models/review.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class WalkerProfile extends StatefulWidget {
  final DogWalker walker_info;
  WalkerProfile({Key key, @required this.walker_info}) : super(key: key);

  @override
  _WalkerProfileState createState() => _WalkerProfileState();
}

class _WalkerProfileState extends State<WalkerProfile> {
  ServiceFacade service = new ServiceFacade();
  List<Review> reviews = [];
  bool loading = true;

  getReviews() async {
    List<Review> _reviews =
        await service.getDogWalkerReviews(widget.walker_info.email);
    setState(() {
      reviews = _reviews;
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getReviews();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          backgroundColor: Colors.transparent,
          middle: const Text('Walker Profile',
              style: TextStyle(color: Colors.white)),
          leading: CupertinoNavigationBarBackButton(
            onPressed: () => Navigator.of(context).pop(),
            color: Colors.white,
          ),
        ),
        child: loading
            ? Container(child: Center(child: CircularProgressIndicator()))
            : Container(
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    // background image and bottom contents
                    Column(
                      children: <Widget>[
                        Container(
                          height: 250.0,
                          color: Color(0xff7bcace),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 70, left: 10, right: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Center(
                                    child: Column(
                                  children: [
                                    Text(widget.walker_info.name,
                                        style: TextStyle(fontSize: 24)),
                                    Text(
                                      widget.walker_info.bio,
                                      textAlign: TextAlign.center,
                                    ),
                                    SmoothStarRating(
                                      rating: widget.walker_info.rating,
                                      size: 30,
                                      starCount: 5,
                                      borderColor: Colors.yellow[800],
                                      color: Colors.yellow[800],
                                      isReadOnly: true,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset('assets/paw.png',
                                            width: 30),
                                        SizedBox(width: 10),
                                        Text(
                                            widget.walker_info.number_walks
                                                    .toString() +
                                                ' Walks',
                                            style: TextStyle(
                                                color: Color(0xFF4f4e79))),
                                      ],
                                    ),
                                    SizedBox(height: 30),
                                  ],
                                )),
                                Text('Reviews',
                                    style: TextStyle(
                                        fontSize: 24,
                                        color: Color(0xFF678EB4))),
                                Container(
                                  height: 250,
                                  child: MediaQuery.removePadding(
                                    context: context,
                                    removeTop: true,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: reviews.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final review = reviews[index];
                                        return Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20, right: 20),
                                          child: Card(
                                              margin: EdgeInsets.only(
                                                  top: 10, bottom: 10),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 5, bottom: 5),
                                                child: ListTile(
                                                  leading: FadeInImage(
                                                    placeholder: AssetImage(
                                                        "assets/placeholders/man.jpg"),
                                                    image: review.user.image !=
                                                            null
                                                        ? FirebaseImage(
                                                            review.user.image)
                                                        : AssetImage(
                                                            "assets/placeholders/man.jpg"),
                                                  ),
                                                  title: Row(
                                                    children: [
                                                      Text(review.user.name),
                                                      SizedBox(width: 5),
                                                      SmoothStarRating(
                                                        rating: review.rating,
                                                        size: 15,
                                                        starCount: 5,
                                                        borderColor:
                                                            Colors.yellow[800],
                                                        color:
                                                            Colors.yellow[800],
                                                        isReadOnly: true,
                                                      )
                                                    ],
                                                  ),
                                                  subtitle: Text(review.body),
                                                ),
                                              )),
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    // Profile image
                    Positioned(
                      top: 150.0,
                      child: Container(
                        height: 150.0,
                        width: 150.0,
                        child: PhysicalModel(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          color: const Color(0xff7c94b6),
                          shape: BoxShape.circle,
                          borderRadius: BorderRadius.circular(30),
                          elevation: 3,
                          child: FadeInImage(
                            placeholder:
                                AssetImage("assets/placeholders/man.jpg"),
                            image: widget.walker_info.image != null
                                ? FirebaseImage(widget.walker_info.image)
                                : AssetImage("assets/placeholders/man.jpg"),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ));
  }
}

class _Review {
  String name;
  String body;
  double rating;
  Image image;

  _Review(String name, String body, double rating, Image image) {
    this.name = name;
    this.body = body;
    this.rating = rating;
    this.image = image;
  }
}
