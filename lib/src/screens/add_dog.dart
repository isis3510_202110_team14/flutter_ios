import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:puphe_ios/src/models/dog.dart';
import 'package:puphe_ios/src/models/puphe_user.dart';

class AddDog extends StatefulWidget {
  final PupheUser user_info;
  AddDog({Key key, @required this.user_info}) : super(key: key);
  @override
  _AddDogState createState() => _AddDogState();
}

class _AddDogState extends State<AddDog> {
  TextEditingController dogNameTextEditingController =
      new TextEditingController();
  TextEditingController dogBreedTextEditingController =
      new TextEditingController();
  TextEditingController dogBioTextEditingController =
      new TextEditingController();
  bool loading = false;
  ServiceFacade authService = new ServiceFacade();

  registerDog() async {
    if (_formKey.currentState.validate()) {
      final dog = new Dog(
        name: dogNameTextEditingController.text,
        breed: dogBreedTextEditingController.text,
        bio: dogBioTextEditingController.text,
        image: "gs://puphe-8dc6d.appspot.com/assets/default/dog.jpg",
      );
      setState(() {
        loading = true;
      });
      authService.addDog(widget.user_info.email, dog).then((result) async {
        if (result != null) {
          showDialog(
              barrierColor: Color(0xff4f4e79),
              context: context,
              builder: (BuildContext dialogContext) {
                return CupertinoAlertDialog(
                  content:
                      Text('Failed to register dog, please try again later'),
                  actions: <Widget>[
                    CupertinoDialogAction(
                        child: Text('Ok'),
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true).pop("Ok");
                        }),
                  ],
                );
              });
          setState(() {
            loading = false;
          });
        } else {
          Navigator.pop(context);
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
      fontSize: 20.0,
    );
    final nameField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        controller: dogNameTextEditingController,
        obscureText: false,
        maxLength: 50,
        style: style,
        validator: RequiredValidator(errorText: 'Your dog\'s name is required'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Enter your dog\'s name",
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final breedField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        controller: dogBreedTextEditingController,
        obscureText: false,
        maxLength: 30,
        style: style,
        validator:
            RequiredValidator(errorText: 'Your dog\'s breed is required'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Enter your dog\'s breed",
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final bioField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        keyboardType: TextInputType.multiline,
        maxLines: null,
        controller: dogBioTextEditingController,
        obscureText: false,
        maxLength: 200,
        style: style,
        validator:
            RequiredValidator(errorText: 'Your dog\'s description is required'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: 'Enter a small description of your dog',
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final addDogButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: Container(
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            registerDog();
          },
          child: Text("Save",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
    );
    final formulario = Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          SizedBox(height: 45.0),
          nameField,
          SizedBox(height: 25.0),
          breedField,
          SizedBox(height: 25.0),
          bioField,
          SizedBox(height: 25.0),
          addDogButon,
          SizedBox(height: 15.0),
        ],
      ),
    );
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: const Text('Register dog'),
      ),
      child: loading
          ? Container(child: Center(child: CircularProgressIndicator()))
          : Container(
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: ListView(children: <Widget>[
                  formulario,
                ]),
              ),
            ),
    );
  }
}
