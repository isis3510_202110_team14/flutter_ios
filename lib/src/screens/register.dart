import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:puphe_ios/src/screens/home_screen.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:puphe_ios/src/models/puphe_user.dart';

class Register extends StatefulWidget {
  final Function show;
  Register(this.show);
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController userNameTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  bool loading = false;
  ServiceFacade authService = new ServiceFacade();

  signUp() async {
    if (_formKey.currentState.validate()) {
      final user = new PupheUser(
        birthday: "",
        name: userNameTextEditingController.text,
        email: emailTextEditingController.text,
        image: "gs://puphe-8dc6d.appspot.com/assets/default/man.jpg",
      );
      setState(() {
        loading = true;
      });
      await authService
          .signUp(emailTextEditingController.text,
              passwordTextEditingController.text)
          .then((result) async {
        if (result.runtimeType != FirebaseAuthException) {
          authService.putUserInfo(user);
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('email', emailTextEditingController.text);
          Navigator.pushReplacement(
              context, CupertinoPageRoute(builder: (context) => HomeScreen()));
        } else {
          String error = "";
          if (result.message != null) {
            setState(() {
              error = result.message;
            });
          } else {
            setState(() {
              error = "Unknown Error";
            });
          }

          showDialog(
              barrierColor: Color(0xff4f4e79),
              context: context,
              builder: (BuildContext dialogContext) {
                return CupertinoAlertDialog(
                  content: Text(error),
                  actions: <Widget>[
                    CupertinoDialogAction(
                        child: Text('Ok'),
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true).pop("Ok");
                          setState(() {
                            loading = false;
                          });
                        }),
                  ],
                );
              });
        }
      });
    }
  }

  bool _showPassword = false;
  bool _showPassword2 = false;
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidate = AutovalidateMode.disabled;
  void _validateInputs() {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = AutovalidateMode.always;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
      fontSize: 20.0,
    );
    final passwordValidator = MultiValidator([
      RequiredValidator(errorText: 'password is required'),
      MinLengthValidator(8,
          errorText: 'password must be at least 8 digits long'),
    ]);
    final nameField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        controller: userNameTextEditingController,
        obscureText: false,
        maxLength: 50,
        style: style,
        validator: RequiredValidator(errorText: 'password is required'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Enter Your name",
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final emailField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        controller: emailTextEditingController,
        obscureText: false,
        maxLength: 50,
        style: style,
        validator: EmailValidator(errorText: 'Enter a valid email'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Enter Email",
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    String _password = '';

    final passwordField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        controller: passwordTextEditingController,
        obscureText: !_showPassword,
        style: style,
        maxLength: 50,
        validator: passwordValidator,
        onChanged: (val) => _password = val,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Enter Password",
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
                color: Colors.blue,
              ),
            ),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final passwordValidation = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        obscureText: !_showPassword2,
        style: style,
        maxLength: 50,
        validator: (val) {
          print(val + " " + _password);
          MatchValidator(errorText: 'Passwords doesn\'nt match')
              .validateMatch(val, _password);
        },
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Enter Password again",
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  _showPassword2 = !_showPassword2;
                });
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
                color: Colors.blue,
              ),
            ),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final registerButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: Container(
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            _validateInputs;
            signUp();
          },
          child: Text("Register",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
    );
    final phoneField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        obscureText: false,
        maxLength: 11,
        style: style,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Enter Phone Number",
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
        keyboardType: TextInputType.phone,
      ),
    );
    final formulario = Form(
      key: _formKey,
      autovalidateMode: _autoValidate,
      child: Column(
        children: <Widget>[
          SizedBox(height: 45.0),
          nameField,
          SizedBox(height: 25.0),
          emailField,
          SizedBox(height: 25.0),
          phoneField,
          SizedBox(height: 25.0),
          passwordField,
          SizedBox(height: 25.0),
          passwordValidation,
          SizedBox(height: 35.0),
          registerButon,
          SizedBox(height: 15.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Already have an account? ",
              ),
              GestureDetector(
                onTap: () {
                  widget.show();
                },
                child: Text(
                  "SignIn now",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      decoration: TextDecoration.underline),
                ),
              ),
            ],
          ),
          // Add TextFormFields and ElevatedButton here.
        ],
      ),
    );
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        automaticallyImplyLeading: false,
        middle: const Text('Register'),
      ),
      child: loading
          ? Container(child: Center(child: CircularProgressIndicator()))
          : Container(
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: ListView(children: <Widget>[
                  formulario,
                ]),
              ),
            ),
    );
  }
}
