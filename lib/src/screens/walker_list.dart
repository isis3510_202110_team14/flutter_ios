import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puphe_ios/src/models/dog_walker.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:puphe_ios/src/screens/walker_profile.dart';

class DogWalkers extends StatefulWidget {
  DogWalkers({Key key}) : super(key: key);

  @override
  _DogWalkersState createState() => _DogWalkersState();
}

class _DogWalkersState extends State<DogWalkers> {
  ServiceFacade service = new ServiceFacade();
  List<DogWalker> dog_walkers = [];
  bool loading = true;
  final int limit = 20;

  getDogWalkers() async {
    List<DogWalker> _dog_walkers = await service.getDogWalkers(limit);
    setState(() {
      dog_walkers = _dog_walkers;
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getDogWalkers();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: const Text('Pet Walkers'),
        ),
        child: loading
            ? Container(child: Center(child: CircularProgressIndicator()))
            : Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: dog_walkers.length,
                  itemBuilder: (BuildContext context, int index) {
                    final dog_walker = dog_walkers[index];
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                              builder: (context) =>
                                  WalkerProfile(walker_info: dog_walker),
                            ));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Card(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: ListTile(
                                leading: FadeInImage(
                                  placeholder:
                                      AssetImage("assets/placeholders/man.jpg"),
                                  image: dog_walker.image != null
                                      ? FirebaseImage(dog_walker.image)
                                      : AssetImage(
                                          "assets/placeholders/man.jpg"),
                                ),
                                title: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        dog_walker.name,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    SizedBox(width: 5),
                                    SmoothStarRating(
                                      rating: dog_walker.rating,
                                      size: 15,
                                      starCount: 5,
                                      borderColor: Colors.yellow[800],
                                      color: Colors.yellow[800],
                                      isReadOnly: true,
                                    ),
                                    SizedBox(width: 5),
                                    Image.asset('assets/paw.png', width: 15),
                                    SizedBox(width: 5),
                                    Text(dog_walker.number_walks.toString(),
                                        style: TextStyle(
                                            color: Color(0xFF4f4e79))),
                                  ],
                                ),
                                subtitle: Text(dog_walker.bio),
                              ),
                            )),
                      ),
                    );
                  },
                ),
              ));
  }
}
