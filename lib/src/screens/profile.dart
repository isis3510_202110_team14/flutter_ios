import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puphe_ios/src/functionalities/login_register.dart';
import 'package:puphe_ios/src/models/dog.dart';
import 'package:puphe_ios/src/models/puphe_user.dart';
import 'package:puphe_ios/src/screens/login.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:puphe_ios/src/screens/add_dog.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  ServiceFacade service = new ServiceFacade();
  PupheUser userInfo;
  bool loading = true;

  List<Dog> dogs = [];

  getUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString('email');
    PupheUser _userInfo = await service.getUserInfo(email);
    List<Dog> _dogs = await service.getUserDogs(email);
    setState(() {
      userInfo = _userInfo;
      dogs = _dogs;
      loading = false;
    });
  }

  signOut() async {
    await service.signOut().then((result) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove('email');
      Navigator.of(context).popUntil((route) => route.isFirst);
      Navigator.of(context, rootNavigator: true).push(
        CupertinoPageRoute<bool>(
          fullscreenDialog: true,
          builder: (BuildContext context) => LoginRegister(),
        ),
      );

      }


    );
  }

  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          middle: const Text('Profile', style: TextStyle(color: Colors.white)),
          trailing: CupertinoButton(
            padding: EdgeInsets.zero,
            child: Text('Logout', style: TextStyle(color: Colors.white)),
            onPressed: () {
              signOut();
            },
          ),
        ),
        child: loading
            ? Container(child: Center(child: CircularProgressIndicator()))
            : Container(
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    // background image and bottom contents
                    Column(
                      children: <Widget>[
                        Container(
                          height: 250.0,
                          color: Color(0xff7bcace),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 70, left: 10, right: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                  child: Text(
                                userInfo.name,
                                style: TextStyle(fontSize: 24),
                              )),
                              Text('Dogs',
                                  style: TextStyle(
                                      fontSize: 24, color: Color(0xFF678EB4))),
                              Column(
                                children: [
                                  Container(
                                      width: double.infinity,
                                      child: Card(
                                        margin: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: CupertinoButton(
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                    builder: (context) =>
                                                        AddDog(
                                                            user_info:
                                                                userInfo),
                                                  )).then((value) async {
                                                List<Dog> _dogs =
                                                    await service.getUserDogs(
                                                        userInfo.email);
                                                setState(() {
                                                  dogs = _dogs;
                                                });
                                              });
                                            },
                                            child: Icon(
                                              CupertinoIcons.add,
                                              size: 40,
                                            )),
                                      )),
                                  SizedBox(
                                    height: 250,
                                    child: MediaQuery.removePadding(
                                      context: context,
                                      removeTop: true,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: dogs.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          final dog = dogs[index];
                                          return Card(
                                              margin: EdgeInsets.only(
                                                  top: 10, bottom: 10),
                                              child: ListTile(
                                                leading: FadeInImage(
                                                  placeholder: AssetImage(
                                                      "assets/placeholders/dog.jpg"),
                                                  image: dog.image != null
                                                      ? FirebaseImage(dog.image)
                                                      : AssetImage(
                                                          "assets/placeholders/dog.jpg"),
                                                ),
                                                title: Text(dog.name),
                                                subtitle: Text(dog.bio),
                                              ));
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    // Profile image
                    Positioned(
                      top: 150.0,
                      child: Container(
                        height: 150.0,
                        width: 150.0,
                        child: PhysicalModel(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          color: const Color(0xff7c94b6),
                          shape: BoxShape.circle,
                          borderRadius: BorderRadius.circular(30),
                          elevation: 3,
                          child: FadeInImage(
                            placeholder:
                                AssetImage("assets/placeholders/man.jpg"),
                            image: userInfo.image != null
                                ? FirebaseImage(userInfo.image)
                                : AssetImage("assets/placeholders/man.jpg"),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ));
  }
}
