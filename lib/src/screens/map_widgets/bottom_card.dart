import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:typed_data';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:puphe_ios/src/models/service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:puphe_ios/src/models/dog.dart';
import 'package:puphe_ios/src/models/dog_walker.dart';
import 'package:puphe_ios/src/screens/rating_dialog.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'dart:math' show acos, asin, cos, pi, pow, sin, sqrt;

typedef void StatusCallback(String status);
class BottomCard extends StatefulWidget {
  final double lat;
  final double long;
  final StatusCallback callBack;
  BottomCard({
    Key,
    key,
    double lat,
    double long,
    @required this.callBack,
  })  : this.lat = lat,
        this.long = long,
        super(key: key);
  @override
  BottomCardState createState() => BottomCardState();
}

class BottomCardState extends State<BottomCard> {
  final firestoreInstance = FirebaseFirestore.instance;
  final ServiceFacade service = new ServiceFacade();
  String message = "Select a dog!";
  List<String> selectedDogs = [];
  String status;
  List<Dog> dogs = [];
  bool loading = true;

  requestService() async {
    if (widget.lat != null &&
        widget.long != null &&
        selectedDogs != [] &&
        selectedDogs.isNotEmpty) {
      String uid = await service.setUserSession();
      await service.requestService(uid, widget.lat, widget.long, selectedDogs);
      if(this.mounted){
      setState(() {
        widget.callBack("Waiting");
      });
      }
    } else if (selectedDogs == [] ||
        selectedDogs.length == 0 ||
        selectedDogs.isEmpty) {
      if (this.mounted) {
        setState(() {
          message = "You need to select a dog";
        });
      }
    } else {
      if (this.mounted) {
        setState(() {
          message = "You need to select a location";
        });
      }
    }
  }

  getDogs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString('email');
    List<Dog> _dogs = await service.getUserDogs(email);
    if (this.mounted) {
      setState(() {
        dogs = _dogs;
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
        future: getDogs(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return new Text('');
            case ConnectionState.waiting:
              return new Text('');
            default:
              if (snapshot.hasError) {
                return new Text('Error: ${snapshot.error}');
              } else {
                return new Container();
              }
          }
        });
    return DraggableScrollableSheet(
        initialChildSize: 0.4,
        minChildSize: 0.3,
        maxChildSize: 0.50,
        builder: (context, scrollController) {
          return NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowGlow();
                return true;
              },
              child: SafeArea(
                bottom: true,
                top: false,
                left: false,
                right: false,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: ClampingScrollPhysics(),
                  child: Container(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height * 0.5,
                    ),
                    color: Colors.white,
                    child: loading
                        ? Container(
                            child: Center(child: CircularProgressIndicator()))
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Center(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[300],
                                    ),
                                    height: 10,
                                    width: 40,
                                  ),
                                ),
                              ),
                              _textDogWidget(text: message),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Container(
                                    child: ElevatedButton(
                                  onPressed: () => requestService(),
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                      Color(0xffa6f2db),
                                    ),
                                  ),
                                  child: Text("Request Service"),
                                )),
                              ),
                              for (var item in dogs)
                                _DogWidget(
                                  id: item.id,
                                  name: item.name,
                                  breed: item.breed,
                                  image: item.image,
                                  bio: item.bio,
                                  isService: false,
                                  callback: (Dog dog, bool status) {
                                    status == true
                                        ? selectedDogs.add(dog.id)
                                        : selectedDogs.remove(dog.id);
                                  },
                                ),
                              futureBuilder,
                            ],
                          ),
                  ),
                ),
              ));
        });
  }
}

class _textDogWidget extends StatelessWidget {
  final String text;
  _textDogWidget({Key key, String text})
      : this.text = text,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Container(
                  child: Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 0),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                this.text,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 24,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
              ),
            ),
          )))
        ],
      ),
    );
  }
}

typedef void DogCallback(Dog dog, bool val);

class _DogWidget extends StatefulWidget {
  final DogCallback callback;
  final String id;
  final String name;
  final String breed;
  final String image;
  final String bio;
  final bool isService;
  const _DogWidget({
    Key key,
    String id,
    String name,
    String breed,
    String image,
    String bio,
    bool isService,
    @required this.callback,
  })  : this.name = name,
        this.breed = breed,
        this.image = image,
        this.bio = bio,
        this.id = id,
        this.isService = isService,
        super(key: key);
  @override
  __DogWidgetState createState() => __DogWidgetState();
}

class __DogWidgetState extends State<_DogWidget> {
  List<bool> flag = [];
  Dog selectedDog;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (this.mounted) {
      setState(() {
        flag.add(false);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Future<Uint8List> getMarket(path) async {
      ByteData byteData = await DefaultAssetBundle.of(context).load(path);
      return byteData.buffer.asUint8List();
    }

    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ListTile(
        leading: Image(
          image: FirebaseImage(widget.image == null
              ? 'gs://puphe-8dc6d.appspot.com/assets/default/dog.jpg'
              : widget.image),
          fit: BoxFit.contain,
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.name,
              style: TextStyle(fontSize: 14, color: Colors.black54),
            ),
            Text(
              widget.breed,
              style: TextStyle(fontSize: 14, color: Colors.black54),
            ),
          ],
        ),
        trailing: widget.isService == false
            ? IconButton(
                icon: new Icon(
                  Icons.add_circle_outline,
                  size: 27,
                ),
                color: flag[0] == false ? Colors.grey : Colors.green,
                onPressed: () {
                  this.setState(() {
                    if (flag[0] == false) {
                      flag[0] = true;
                      selectedDog = new Dog(
                          id: widget.id,
                          name: widget.name,
                          breed: widget.breed,
                          bio: widget.bio,
                          image: null);
                      widget.callback(selectedDog, true);
                    } else {
                      flag[0] = false;
                      widget.callback(selectedDog, false);
                      selectedDog = null;
                    }
                  });
                },
              )
            : IconButton(
                icon: new Icon(
                  Icons.alarm_on,
                  size: 27,
                ),
                color: Colors.green,
              ),
      ),
    );
  }
}

class WaitingBottomCard extends StatefulWidget {
  final StatusCallback callback;
  final String status;
  final String user;
  WaitingBottomCard(
      {Key key, String status, String user, @required this.callback})
      : this.status = status,
        this.user = user,
        super(key: key);
  @override
  _WaitingBottomCardState createState() => _WaitingBottomCardState();
}

class _WaitingBottomCardState extends State<WaitingBottomCard> {
  ServiceFacade serviceFacade = new ServiceFacade();
  Service serviceUser;
  List<Dog> dogs = [];
  bool loading = true;
  Widget textStateWidget() {
    if (widget.status == "Waiting") {
      return _textDogWidget(text: "Waiting for a pet walker");
    } else if (widget.status == "Accepted") {
      return _textDogWidget(
          text: "Service acepted, please wait for the petwalker to arrive");
    } else if (widget.status == "Ongoing") {
      return _textDogWidget(text: "Service on going");
    } else if (widget.status == "Finished") {
      return _textDogWidget(text: "Please rate the pet walker service");
    }
  }

  getCurrentService() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString('email');
    List<Dog> serviceDogs = [];
    List<Dog> allDogs = await serviceFacade.getUserDogs(email);
    Service value = await serviceFacade.getUserService(widget.user);
    if (value != null) {
      Dog checkedDog;
      for(int i=0;i<allDogs.length;i++)
      {
        checkedDog=allDogs[i];
        if (value.dogs.contains(checkedDog.id)) {
          serviceDogs.add(checkedDog);
        }
      }
      setState(() {
        loading = false;
        serviceUser = value;
        dogs = serviceDogs;
        widget.callback(serviceUser.status);
      });
    }
  }

  Widget buttonStateWidget() {
    if (widget.status == "Waiting") {
      return ElevatedButton(
        onPressed: () => cancelService(),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
            Color(0xffa6f2db),
          ),
        ),
        child: Text("Cancer Service"),
      );
    } else if (widget.status == "Accepted") {
      return ElevatedButton(
        onPressed: () => cancelService(),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
            Color(0xffa6f2db),
          ),
        ),
        child: Text("Cancer Service"),
      );
    }
  }

  cancelService() async {
    setState(() {
      widget.callback("No service");
    });
    await serviceFacade.cancelService(widget.user);
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
        future: getCurrentService(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return new Text("");
            case ConnectionState.waiting:
              return new Text('');
            default:
              if (snapshot.hasError) {
                return new Text('Error: ${snapshot.error}');
              } else {
                return new Container();
              }
          }
        });
    return DraggableScrollableSheet(
        initialChildSize: 0.4,
        minChildSize: 0.3,
        maxChildSize: 0.50,
        builder: (context, scrollController) {
          return NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
              return true;
            },
            child: SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: SingleChildScrollView(
                controller: scrollController,
                physics: ClampingScrollPhysics(),
                child: Container(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height * 0.5,
                    ),
                    color: Colors.white,
                    child: loading
                        ? Container(
                            child: Center(child: CircularProgressIndicator()))
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Center(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[300],
                                    ),
                                    height: 10,
                                    width: 40,
                                  ),
                                ),
                              ),
                              textStateWidget(),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Container(child: buttonStateWidget()),
                              ),
                              futureBuilder,
                              for (var item in dogs)
                                _DogWidget(
                                  id: item.id,
                                  name: item.name,
                                  breed: item.breed,
                                  image: item.image,
                                  bio: item.bio,
                                  isService: true,
                                ),
                            ],
                          )),
              ),
            ),
          );
        });
  }
}

typedef void UserLocationCallback(Service service);

class ServiceBottomCard extends StatefulWidget {
  final UserLocationCallback callback;
  final String user;
  final String status;
  ServiceBottomCard({
    Key key,
    String user,
    String status,
    @required this.callback,
  })  : this.user = user,
        this.status = status,
        super(key: key);
  @override
  _ServiceBottomCardState createState() => _ServiceBottomCardState();
}

class _ServiceBottomCardState extends State<ServiceBottomCard> {
  ServiceFacade serviceFacade = new ServiceFacade();
  Service serviceUser;
  List<Dog> dogs = [];
  DogWalker dogWalker;
  bool loading = true;
  double roundDouble(double value, int places){
    double mod = pow(10.0, places);
    return ((value * mod).round().toDouble() / mod);
  }
  Widget textStateWidget() {
    Widget widgets;
    if (widget.status == "Accepted") {
      widgets=_textDogWidget(
          text: "Service accepted, please wait for the petwalker to arrive");
    } else if (widget.status == "Ongoing") {
      widgets= _textDogWidget(text: "Service on going");
    } else if (widget.status=="Returning"){
      double dist = Geolocator.distanceBetween(serviceUser.location.latitude,serviceUser.location.longitude,serviceUser.walker_location.latitude,serviceUser.walker_location.longitude);
      if(dist<=750){
        widgets= _textDogWidget(text:"Your pet walker is ${roundDouble(dist,3)} meters near");
      }
      else{
        widgets= _textDogWidget(text:"Your pet walker is returning");
      }
    }
    return widgets;
  }

  getCurrentService() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString('email');
    List<Dog> serviceDogs = [];
    List<Dog> allDogs = await serviceFacade.getUserDogs(email);
    Service value = await serviceFacade.getUserService(widget.user);
    Dog checkedDog;
    for(int i=0;i<allDogs.length;i++)
    {
      checkedDog=allDogs[i];
      if (value.dogs.contains(checkedDog.id)) {
        serviceDogs.add(checkedDog);
      }
    }
    DogWalker walker = await serviceFacade.getPetWalker(value.dog_walker);
    if(this.mounted){
    setState(() {
      dogWalker = walker;
      serviceUser = value;
      dogs = serviceDogs;
      loading = false;
    });
    }

  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
        future: getCurrentService(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return new Text("");
            case ConnectionState.waiting:
              return new Text('');
            default:
              if (snapshot.hasError) {
                return new Text('Error: ${snapshot.error}');
              } else {
                return new Container();
              }
          }
        });
    return DraggableScrollableSheet(
        initialChildSize: 0.4,
        minChildSize: 0.3,
        maxChildSize: 0.50,
        builder: (context, scrollController) {
          return NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
              return true;
            },
            child: SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: SingleChildScrollView(
                controller: scrollController,
                physics: ClampingScrollPhysics(),
                child: Container(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height * 0.5,
                    ),
                    color: Colors.white,
                    child: loading
                        ? Container(
                            child: Center(child: CircularProgressIndicator()))
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Center(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[300],
                                    ),
                                    height: 10,
                                    width: 40,
                                  ),
                                ),
                              ),
                              textStateWidget(),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Container(
                                    //child: ,
                                    ),
                              ),
                              futureBuilder,
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 20, right: 20),
                                child: Card(
                                    margin: EdgeInsets.only(bottom: 20),
                                    child: ListTile(
                                      leading: FadeInImage(
                                        placeholder: AssetImage(
                                            "assets/placeholders/man.jpg"),
                                        image: dogWalker.image != null
                                            ? FirebaseImage(dogWalker.image)
                                            : AssetImage(
                                                "assets/placeholders/man.jpg"),
                                      ),
                                      title: Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              dogWalker.name,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          SizedBox(width: 5),
                                          SmoothStarRating(
                                            rating: dogWalker.rating==null?0:dogWalker.rating,
                                            size: 15,
                                            starCount: 5,
                                            borderColor: Colors.yellow[800],
                                            color: Colors.yellow[800],
                                            isReadOnly: true,
                                          ),
                                          SizedBox(width: 5),
                                          Image.asset('assets/paw.png',
                                              width: 15),
                                          SizedBox(width: 5),
                                          Text(
                                              dogWalker.number_walks==null?0.toString():dogWalker.number_walks.toString(),
                                              style: TextStyle(
                                                  color: Color(0xFF4f4e79))),
                                        ],
                                      ),
                                      subtitle: Text(dogWalker.bio),
                                    )),
                              ),
                              for (var item in dogs)
                                _DogWidget(
                                  id: item.id,
                                  name: item.name,
                                  breed: item.breed,
                                  image: item.image,
                                  bio: item.bio,
                                  isService: true,
                                ),
                            ],
                          )),
              ),
            ),
          );
        });
  }
}

class FinishedBottomCard extends StatelessWidget {
  final String user;
  final Service service;
  FinishedBottomCard({Key key, String user, Service service})
      : this.user = user,
        this.service = service,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
        initialChildSize: 0.28,
        minChildSize: 0.28,
        maxChildSize: 0.28,
        builder: (context, scrollController) {
          return NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
              return true;
            },
            child: SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: SingleChildScrollView(
                controller: scrollController,
                physics: ClampingScrollPhysics(),
                child: Container(
                    constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height * 0.5,
                    ),
                    color: Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Center(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.grey[300],
                              ),
                              height: 10,
                              width: 40,
                            ),
                          ),
                        ),
                        _textDogWidget(text: "Please rate the service"),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Container(
                              child: ElevatedButton(
                            onPressed: () {
                              showDialog(
                                  barrierColor: Color(0xff4f4e79),
                                  context: context,
                                  builder: (BuildContext dialogContext) {
                                    return MyRatingDialog(
                                        user: user, service: service);
                                  });
                              Navigator.of(context).pop();
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                Color(0xffa6f2db),
                              ),
                            ),
                            child: Text("Rate Service"),
                          )),
                        ),
                      ],
                    )),
              ),
            ),
          );
        });
  }
}
