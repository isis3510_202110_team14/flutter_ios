import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:puphe_ios/src/screens/home_screen.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  final Function show;
  Login(this.show);
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController emailEditingController = new TextEditingController();
  TextEditingController passwordEditingController = new TextEditingController();
  bool loading = false;

  ServiceFacade authService = new ServiceFacade();
  final formKey = GlobalKey<FormState>();

  signIn() async {
    if (formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      await authService
          .signIn(emailEditingController.text, passwordEditingController.text)
          .then((result) async {
        if (result.runtimeType != FirebaseAuthException) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('email', emailEditingController.text);
          Navigator.pushReplacement(
              context, CupertinoPageRoute(builder: (context) => HomeScreen()));
        } else {
          setState(() {
            loading = false;
          });

          String error = "";
          if (result.message != null) {
            setState(() {
              error = result.message;
            });
          } else {
            setState(() {
              error = "Cannot login, please try again";
            });
          }

          showDialog(
              barrierColor: Color(0xff4f4e79),
              context: context,
              builder: (BuildContext dialogContext) {
                return CupertinoAlertDialog(
                  content: Text(error),
                  actions: <Widget>[
                    CupertinoDialogAction(
                        child: Text('Ok'),
                        onPressed: () {
                          Navigator.of(context, rootNavigator: true).pop("Ok");
                        }),
                  ],
                );
              });
        }
      });
    }
  }

  bool _showPassword = false;
  void _togglevisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
      fontSize: 20.0,
    );
    final passwordValidator = MultiValidator([
      RequiredValidator(errorText: 'password is required'),
      MinLengthValidator(8,
          errorText: 'password must be at least 8 digits long'),
    ]);
    final emailField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        controller: emailEditingController,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        obscureText: false,
        maxLength: 50,
        style: style,
        validator: EmailValidator(errorText: 'Enter a valid email'),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Email",
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final passwordField = Material(
      color: CupertinoTheme.of(context).scaffoldBackgroundColor,
      shadowColor: CupertinoTheme.of(context).scaffoldBackgroundColor,
      child: TextFormField(
        controller: passwordEditingController,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        obscureText: !_showPassword,
        style: style,
        maxLength: 50,
        validator: passwordValidator,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: "Password",
            suffixIcon: GestureDetector(
              onTap: () {
                _togglevisibility();
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
                color: Colors.blue,
              ),
            ),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: Container(
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            signIn();
          },
          child: Text("Log in",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
    );

    final forgotPassword = GestureDetector(
        child: Text("Forgot Your Password?",
            textAlign: TextAlign.center,
            style: TextStyle(
                decoration: TextDecoration.underline, color: Colors.blue)),
        onTap: () {
          // do what you need to do when "Click here" gets clicked
        });

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        automaticallyImplyLeading: false,
        middle: const Text('Log in'),
      ),
      child: loading
          ? Container(child: Center(child: CircularProgressIndicator()))
          : Container(
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Form(
                  key: formKey,
                  child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(
                            height: 100,
                          ),
                          SizedBox(height: 45.0),
                          emailField,
                          SizedBox(height: 25.0),
                          passwordField,
                          SizedBox(
                            height: 35.0,
                          ),
                          SizedBox(
                            height: 155.0,
                            child: Image.asset(
                              "assets/dog_walker.png",
                              fit: BoxFit.contain,
                            ),
                          ),
                          loginButon,
                          SizedBox(
                            height: 15.0,
                          ),
                          forgotPassword,
                          SizedBox(
                            height: 16,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Don't have account? ",
                              ),
                              GestureDetector(
                                onTap: () {
                                  widget.show();
                                },
                                child: Text(
                                  "Register now",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      decoration: TextDecoration.underline),
                                ),
                              ),
                            ],
                          ),
                        ]),
                  ),
                ),
              ),
            ),
    );
  }
}
