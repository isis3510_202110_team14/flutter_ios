import 'package:flutter/cupertino.dart';
import 'package:puphe_ios/src/functionalities/daily_reminder.dart';
import 'package:puphe_ios/src/services/service_facade.dart';
import 'map.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  double averageTime;
  ServiceFacade services = new ServiceFacade();
  setAverageTime() async {
    double time = await services.getAcceptanceAverageTime();

    setState(() {
      this.averageTime = time;
    });
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = FutureBuilder(
        future: setAverageTime(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return new Container();
            case ConnectionState.waiting:
              return new Container();
            default:
              if (snapshot.hasError) {
                return new Container();
              } else {
                return new Container();
              }
          }
        });
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: const Text('Home Screen'),
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset("assets/dog_walker.png"),
              Text(
                'Welcome to Puphe!!!',
                style:
                    CupertinoTheme.of(context).textTheme.navLargeTitleTextStyle,
              ),
              SizedBox(height: 32),
              CupertinoButton(
                child: Text('Request a service!'),
                color: Color(0xFF4f4e79),
                onPressed: () {
                  Navigator.push(
                      context,
                      CupertinoPageRoute(
                        builder: (context) => Map(),
                      ));
                },
              ),
              SizedBox(height: 15),
              Text(
                'Average service response time: ${this.averageTime==null?"Loading":this.averageTime} minutes',
                style: TextStyle(fontSize: 15),
              ),
              futureBuilder
            ].toList(),
          ),
        ));
  }
}
