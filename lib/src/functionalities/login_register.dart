import 'package:flutter/material.dart';
import 'package:puphe_ios/src/screens/login.dart';
import 'package:puphe_ios/src/screens/register.dart';

class LoginRegister extends StatefulWidget {
  @override
  _LoginRegisterState createState() => _LoginRegisterState();
}

class _LoginRegisterState extends State<LoginRegister> {
  bool showLogin = true;

  void show()
  {
    setState(() {
      showLogin = !showLogin;
    });
  }
  @override
  Widget build(BuildContext context) {
    if(showLogin)
      {
        return Login(show);
      }
    else
      {
        return Register(show);
      }
  }
}
