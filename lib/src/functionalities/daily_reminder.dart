import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../services/service_facade.dart';

class DailyReminder {
  FlutterLocalNotificationsPlugin fltrNotification;
  final androidInitilize = new AndroidInitializationSettings('ic_launcher');
  final iOSinitilize = new IOSInitializationSettings();
  final facade = new ServiceFacade();

  Future schedule(String email, String dog) async {
    final _dog = await facade.getUserDog(email, dog);
    final String currentTimeZone =
        await FlutterNativeTimezone.getLocalTimezone();
    tz.initializeTimeZones();
    tz.setLocalLocation(tz.getLocation(currentTimeZone));
    final initilizationsSettings = new InitializationSettings(
        android: androidInitilize, iOS: iOSinitilize);
    fltrNotification = new FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initilizationsSettings,
        onSelectNotification: notificationSelected);
    var androidDetails = new AndroidNotificationDetails(
        "Channel ID", "Desi programmer", "This is my channel",
        importance: Importance.max);
    var iSODetails = new IOSNotificationDetails();
    var generalNotificationDetails =
        new NotificationDetails(android: androidDetails, iOS: iSODetails);
    await fltrNotification.zonedSchedule(
        0,
        'Friendly reminder',
        'Hey!!! it seems that ${_dog.name} needs a walk',
        tz.TZDateTime.now(tz.local).add(const Duration(seconds: 5)),
        generalNotificationDetails,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime);
  }

  Future notificationSelected(String payload) async {}
}
