import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:puphe_ios/src/models/dog.dart';
import 'dart:async' show Future;
import 'dart:math';
import 'package:puphe_ios/src/models/dog_walker.dart';
import 'package:puphe_ios/src/models/puphe_user.dart';
import 'package:puphe_ios/src/models/review.dart';

import '../models/dog.dart';
import '../models/dog.dart';
import '../models/puphe_user.dart';

class DatabaseFunctions {
  final _firestore = FirebaseFirestore.instance;

  void postUserInfo(PupheUser user) {
    _firestore.collection("user").doc(user.email).set(user.toMap());
  }

  Future addDog(String email, Dog dog) async {
    try {
      await _firestore
          .collection("user")
          .doc(email)
          .collection("dogs")
          .doc()
          .set(dog.toMap());
    } catch (e) {
      return e;
    }
  }

  Future<PupheUser> getUserInfo(String email) async {
    final data = await _firestore.collection("user").doc(email).get();
    return new PupheUser.fromJSON(data.data());
  }

  Future<Dog> getUserDog(String email, String dog) async {
    final data = await _firestore
        .collection("user")
        .doc(email)
        .collection("dogs")
        .doc(dog)
        .get();
    return new Dog.fromJSON(data.data(), dog);
  }

  Future<List<Dog>> getUserDogs(String email) async {
    var data =
        await _firestore.collection("user").doc(email).collection("dogs").get();
    return new List<Dog>.generate(data.docs.length,
        (i) => new Dog.fromJSON(data.docs[i].data(), data.docs[i].id));
  }

  Future<List<DogWalker>> getDogWalkers(int limit) async {
    final data = await _firestore
        .collection("dog_walker")
        .orderBy('rating', descending: true)
        .limit(limit)
        .get();
    return new List<DogWalker>.generate(
        data.docs.length, (i) => new DogWalker.fromJSON(data.docs[i].data()));
  }

  Future<DogWalker> getDogWalker(String email) async {
    final data = await _firestore.collection("dog_walker").doc(email).get();
    return new DogWalker.fromJSON(data.data());
  }

  Future<List<Review>> getDogWalkerReviews(String email) async {
    final data = await _firestore
        .collection("dog_walker")
        .doc(email)
        .collection("reviews")
        .get();
    List<Review> reviews = [];
    var doc;
    for (int i=0;i<data.docs.length;i++) {
      doc=data.docs[i];
      final review = doc.data();
      await review['user'].get().then((user) => review['user'] = user.data());
      reviews.add(new Review.fromJSON(review));
    }
    return reviews;
  }

  Future<double> getAverageAcceptServiceTime() async {
    final data = await _firestore.collection("trip").get();
    double totalTime = 0;
    var doc;
    for (int i=0; i<data.docs.length;i++) {
      doc=data.docs[i];
      DateTime request = DateTime.parse(doc["requestingDate"]);
      DateTime accepted = DateTime.parse(doc["acceptingDate"]);
      int difference = accepted.difference(request).inSeconds;
      totalTime += difference;
    }
    return roundDouble((totalTime / data.docs.length) / 60, 2);
  }

  double roundDouble(double value, int places) {
    double mod = pow(10.0, places);
    return ((value * mod).round().toDouble() / mod);
  }
}
