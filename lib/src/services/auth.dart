import 'package:firebase_auth/firebase_auth.dart';
import 'package:puphe_ios/src/models/puphe_user.dart';

class AuthMethods {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future signInWithEmail(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      return e;
    }
  }

  Future signUpWithEmail(String email, String password) async {
    try {
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
    } catch (e) {
      return e;
    }
  }

  Future signOut() async {
    try {

      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<String> getCurrentLoggeduser() async {
    var user = await FirebaseAuth.instance.currentUser;
    if (user != null) {
      return user.uid;
    } else {
      return "Not logged in";
    }
  }
}
