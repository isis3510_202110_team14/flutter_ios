import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:puphe_ios/src/models/service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';

class GeolocationService {
  final _geo = Geoflutterfire();
  final _firestore = FirebaseFirestore.instance;
  final _realTimedb = FirebaseDatabase.instance;

  Future requestService(
      String uid, List<String> dogs, double lat, double long) async {
    if (uid != null) {
      final ref = _realTimedb.reference();
      DateTime dateToday = DateTime.now();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String user = prefs.getString('email');

      var response = await ref.child("trip").child(uid).set({
        "status": "Waiting",
        "requestingDate": dateToday.toString(),
        "acceptingDate": "",
        "startingDate": "",
        "endingDate": "",
        "duration": "",
        "rating": "",
        "cost": dogs.length * 9000,
        "comment": "",
        "dogWalker": "",
        "user": user,
        "dogs": dogs,
        "location": {'latitude': lat, 'longitude': long},
        "petWalkerLocation": {'latitude': 0,
          'longitude': 0}
      });
      return response;
    }
  }

  Future<Service> getUserService(String user) async {
    Service service = null;
    var data = await _realTimedb.reference().child("trip").child(user).once();
    if (data.value != null) {
      service = new Service.fromJSON(data.value, user);
      return service;
    }
  }

  Future cancelService(String user) async {
    try {
      String answer = "";
      if (user != null) {
        final ref = _realTimedb.reference();
        await ref.child("trip").child(user).remove();
        answer = "Trip Canceled";
      } else {
        answer = "Trip cannot be canceled";
      }
      return answer;
    } catch (e) {
      print(e);
      return "Trip cannot be canceled";
    }
  }

  Future getServiceStatus(String user) async {
    try {
      String answer = "";
      if (user != null) {
        final ref = _realTimedb.reference();
        var response = await ref
            .child("trip")
            .child(user)
            .once()
            .then((DataSnapshot snap) {
          if (snap.value == null) {
            answer = "No service";
          } else {
            answer = snap.value["status"];
          }
        });
      } else {
        answer = "No service";
      }
      return answer;
    } catch (e) {
      return "No Service";
    }
  }

  Future finishService(String user, Service service) async {
    final endingDate = DateTime.now().toString();
    final duration = DateTime.now().difference(service.startingDate).inMinutes;
    final user_ref = _firestore.collection("user").doc(service.user);
    final udog_walker_ref =
        _firestore.collection("dog_walker").doc(service.dog_walker);
    final dogs_ref = new List.generate(service.dogs.length,
        (i) => user_ref.collection("dogs").doc(service.dogs[i]));
    Map<String, dynamic> data = {
      "status": "Finished",
      "requestingDate": service.requestingDate.toString(),
      "acceptingDate": service.acceptingDate.toString(),
      "startingDate": service.startingDate.toString(),
      "endingDate": endingDate,
      "duration": duration,
      "rating": service.rating,
      "cost": service.dogs.length * 150 * duration,
      "comment": service.comment,
      "dogWalker": udog_walker_ref,
      "user": user_ref,
      "dogs": dogs_ref,
      "location": {
        'latitude': service.location.latitude,
        'longitude': service.location.longitude
      },
      "petWalkerLocation": {
        'latitude': service.walker_location.latitude,
        'longitude': service.walker_location.longitude
      }
    };
    await _firestore.collection("trip").add(data);
    final ref = _realTimedb.reference();
    await ref.child("trip").child(user).remove();
  }
}
