import 'package:puphe_ios/src/models/service.dart';
import 'package:puphe_ios/src/services/auth.dart';
import 'package:puphe_ios/src/services/geolocation_services.dart';
import 'package:puphe_ios/src/services/database_functions.dart';

import '../models/dog.dart';
import '../models/puphe_user.dart';

class ServiceFacade {
  final _auth = AuthMethods();
  final _dbFunctions = DatabaseFunctions();
  final _geolocation_service = GeolocationService();
  String userSession;

  Future requestService(
      String user, double lat, double long, List<String> dogs) {
    return _geolocation_service.requestService(user, dogs, lat, long);
  }

  Future cancelService(String user) {
    return _geolocation_service.cancelService(user);
  }

  Future getServiceStatus(String user) {
    var x = _geolocation_service.getServiceStatus(user);
    return x;
  }

  Future putUserInfo(PupheUser user) {
    _dbFunctions.postUserInfo(user);
  }

  Future addDog(String email, Dog dog) {
    return _dbFunctions.addDog(email, dog);
  }

  Future getUserInfo(String email) {
    return _dbFunctions.getUserInfo(email);
  }

  Future getUserDogs(String email) {
    return _dbFunctions.getUserDogs(email);
  }

  Future getDogWalkers(limit) {
    return _dbFunctions.getDogWalkers(limit);
  }

  Future getDogWalkerReviews(String email) {
    return _dbFunctions.getDogWalkerReviews(email);
  }

  Future signIn(String email, String password) {
    return _auth.signInWithEmail(email, password);
  }

  Future signUp(String email, String password) {
    return _auth.signUpWithEmail(email, password);
  }

  Future signOut() {
    return _auth.signOut();
  }

  Future<Service> getUserService(String user) {
    return _geolocation_service.getUserService(user);
  }

  Future getPetWalker(String walker) {
    return _dbFunctions.getDogWalker(walker);
  }

  Future setUserSession() async {
    await _auth.getCurrentLoggeduser().then((value) => userSession = value);
    return userSession;
  }

  String getCurrentLoggedInUser() {
    setUserSession();
    return userSession;
  }

  Future finishService(String user, Service service) {
    return _geolocation_service.finishService(user, service);
  }

  Future<double> getAcceptanceAverageTime() {
    return _dbFunctions.getAverageAcceptServiceTime();
  }

  Future<Dog> getUserDog(String email, String dog) {
    return _dbFunctions.getUserDog(email, dog);
  }
}
