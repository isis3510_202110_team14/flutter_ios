import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puphe_ios/src/functionalities/login_register.dart';
import 'package:puphe_ios/src/screens/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var email = prefs.getString('email');
  runApp(CupertinoApp(
      title: 'Puphe',
      theme: CupertinoThemeData(brightness: Brightness.light),
      home: email == null ? LoginRegister() : HomeScreen(),
      localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
        DefaultMaterialLocalizations.delegate,
        DefaultWidgetsLocalizations.delegate,
      ]));
}
